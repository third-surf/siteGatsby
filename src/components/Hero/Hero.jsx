import React, { Component } from "react";
import Img from "gatsby-image";
import Card from "react-md/lib/Cards/Card";
import CardText from "react-md/lib/Cards/CardText";
import UserLinks from "../UserLinks/UserLinks";
import config from "../../../data/SiteConfig";
import "./Hero.scss";


class Hero extends Component {
  render() {
    const heroImageSizes = this.props.heroImage.sizes;
    return (
      <div className="about-container md-grid mobile-fix">
        <Card className="md-grid md-cell--8">
          <div className="hero-wrapper">
            <Img
              title="Main image"
              alt="pumping surf"
              className="hero-img"
              sizes={heroImageSizes}
            />
            <CardText>
              <p className="hero-text md-body-1">{config.userDescription}</p>
            </CardText>
            
            <UserLinks labeled config={config} />
          </div>
        </Card>
      </div>
    );
  }
}

export default Hero;
