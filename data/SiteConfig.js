module.exports = {
  blogPostDir: "sample-posts", // Directory name of my posts/articles.
  imagesDir: "images", // Directory name for where my images are stored.
  siteTitle: "Third Surf", // Title of this site.
  siteTitleAlt: "Surf adventures, advice, coaching and tours.", // Alternative site title for SEO.
  siteLogo: "/logos/logo-1024.png", // Logo used for SEO and manifest.
  siteUrl: "http://thirdsurf.com", // Domain of your website without pathPrefix.
  pathPrefix: "/shred", // Prefixes all links. For cases when deployed to example.github.io/gatsby-material-starter/.
  fixedFooter: false, // Whether the footer component is fixed, i.e. always visible
  siteDescription: "A school of surfing for everyone.", // Website description used for RSS feeds/meta description tag.
  siteRss: "/rss.xml", // Path to the RSS file.
  siteFBAppID: "", // FB Application ID for using app insights
  siteGATrackingID: "", // Tracking code ID for google analytics.
  disqusShortname: "", // Disqus shortname.
  postDefaultCategoryID: "Surfing", // Default category for posts.
  userName: "Todder", // Username to display in the author segment.
  userTwitter: "thehomie", // Optionally renders "Follow Me" in the UserInfo segment.
  userLocation: "Santa Cruz, California", // User location to display in the author segment.
  userAvatar: "https://api.adorable.io/avatars/150/test.png", // User avatar to display in the author segment.
  userDescription:
    "Polyglot athlete, scientist, philosopher & surfer. Rescue teams, professional driving, internet technology, the maker revolution, ", // User description to display in the author segment.
  // Links to social profiles/projects you want to display in the author segment/navigation bar.
  userLinks: [
    {
      label: "GitHub",
      url: "https://github.com/roddle",
      iconClassName: "fa fa-github"
    },
    {
      label: "Twitter",
      url: "https://twitter.com/thehomie",
      iconClassName: "fa fa-twitter"
    },
    {
      label: "Email",
      url: "mailto:andetodd@gmail.com",
      iconClassName: "fa fa-envelope"
    }
  ],
  copyright: "Copyright © 2018. Todd Scott Anderson" // Copyright string for the footer of the website and RSS feed.
};
